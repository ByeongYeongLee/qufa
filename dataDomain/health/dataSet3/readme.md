#### 3차년도 보건 데이터

- health2_heart_risk10_origin.csv : 심장병질환예측 위한 원본 데이터셋
- health2_heart_risk10_gan.csv : 원본데이터를 기초로, GAN 기반의 모조 데이터셋
- health2_after.csv : 원본과 모조 데이터를 합친 후 편향성 제거에 따른 보정 후 데이터셋
