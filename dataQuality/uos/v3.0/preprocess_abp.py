import pandas as pd 


if __name__ == "__main__":
    file_path = "data/abp_data.csv"
    data = pd.read_csv(file_path, index_col=0)

    data.index = data.index.astype(str)
    print(data.head(10))

    time_indexes = []
    for i, idx in enumerate(data.index):
        idx1 = "%0.2f" % data['time'][i]
        timedata = idx1.split(".")
        hh = int(timedata[0]) // 3600
        mm = (int(timedata[0]) // 60) % 60
        ss = int(timedata[0]) % 60
        ms = int(timedata[1])

        time_index = f"2021-01-01 {hh:02d}:{mm:02d}:{ss:02d}.{ms:02d}"
        time_index = pd.to_datetime(time_index)
        time_indexes.append(time_index)

        if i % 10000 == 0:
            print(i)

    data.index = time_indexes

    data.to_csv("data/abp_data_prepocess.csv")
              
    print(data.shape)