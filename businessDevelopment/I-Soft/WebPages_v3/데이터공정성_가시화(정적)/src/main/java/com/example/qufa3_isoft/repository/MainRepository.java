package com.example.qufa3_isoft.repository;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Mapper
@Repository
public interface MainRepository {
    String getNow(Map map);
}

