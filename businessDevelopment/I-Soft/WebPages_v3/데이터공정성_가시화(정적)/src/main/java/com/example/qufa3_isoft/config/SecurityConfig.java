package com.example.qufa3_isoft.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.MessageDigestPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import javax.sql.DataSource;

@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeHttpRequests((authorize) -> authorize
//                                .antMatchers("/pw").permitAll()
                                .antMatchers("/resources/**").permitAll()
                                .antMatchers("/**").hasRole("USER")
                                .antMatchers("/admin/**").hasRole("ADMIN")
                )
//                .formLogin(withDefaults());
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/login/process")
                .defaultSuccessUrl("/")
                .failureHandler(customAuthFailureHandler())
                .permitAll();
        return http.build();
    }

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring()
                .antMatchers("/common/**")
                .antMatchers("/login**")
                .antMatchers("/main/case2/**", "/main/case1/**")
                .antMatchers("/ignore1", "/ignore2");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
        return new MessageDigestPasswordEncoder("SHA-256");
    }

    @Bean
    public CustomAuthFailureHandler customAuthFailureHandler() {
        return new CustomAuthFailureHandler();
    }

    // case 1: jdbc
    @Bean
    public UserDetailsManager users(DataSource dataSource) {
        JdbcUserDetailsManager users = new JdbcUserDetailsManager(dataSource);
        users.setUsersByUsernameQuery("SELECT user_id, user_pw, enabled FROM user WHERE user_id = ?");
        users.setAuthoritiesByUsernameQuery("SELECT user_id, role FROM role WHERE user_id = ?");
        return users;
    }

    // case 2: in-memory
//    @Bean
//    public InMemoryUserDetailsManager guestUsers() {
//        UserDetails user1 = User.withDefaultPasswordEncoder()
//                .username("guest1")
//                .password("1234")
//                .roles("USER")
//                .build();
//        UserDetails user2 = User.withDefaultPasswordEncoder()
//                .username("guest2")
//                .password("1234")
//                .roles("USER")
//                .build();
//        return new InMemoryUserDetailsManager(user1, user2);
//    }

}

