import os

from flask import Flask, request, abort, jsonify, send_from_directory

Storage = "/app/files"

API_KEY_1 = 'aGVsbG93b3JsZGdvZGRheW1ibmNza3dlaWRrY24='


app = Flask(__name__)


@app.route("/api/download/list")
def list_files():
    """Endpoint to list files on the server."""
    files = []
    for filename in os.listdir(Storage):
        path = os.path.join(Storage, filename)
        if os.path.isfile(path):
            files.append(filename)
            files.sort()
    return jsonify(files)


@app.route("/api/downlad/port", methods = ['GET'])
def get_port():
    """Download a port Data"""
    key = request.args.get('key')
    file = request.args.get('file')
    if key != API_KEY_1:
        abort(404, "check api key or filename")
    else:
        return send_from_directory(Storage, file, as_attachment=True), abort(200, "Succes")
@app.route("/api/download", methods = ['GET'])
def get_download():
    """Download a file."""
    key = request.args.get('key')
    file = request.args.get('file') 
    if key != API_KEY_1:
        abort(404, "check api key or filename")
    else:
        return send_from_directory(Storage,file , as_attachment=True)

#Upload file
#@app.route("/Users/<filename>", methods=["POST"])
#def post_file(filename):
#    """Upload a file."""
#
#    if "/" in filename:
#        #Return 400 BAD REQUEST
#        abort(400, "no subdirectories allowed")
#
#    with open(os.path.join(Storage, filename), "wb") as fp:
#        fp.write(request.data)
#
    #Return 201 CREATED
#    return "", 201


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=28080)
